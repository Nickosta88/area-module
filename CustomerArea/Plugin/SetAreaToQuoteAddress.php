<?php

declare(strict_types=1);

namespace Elogic\CustomerArea\Plugin;

use Magento\Quote\Api\Data\AddressInterface;

class SetAreaToQuoteAddress
{
    /**
     * @param AddressInterface $address
     * @return void
     */
    protected function addArea(AddressInterface $address)
    {
        $extAttr = $address->getExtensionAttributes();
        if (!empty($extAttr)) {
                $area = $extAttr->getArea();
                if (!empty($area)) {
                    $address->setArea($area);
                }
        }
    }
}
