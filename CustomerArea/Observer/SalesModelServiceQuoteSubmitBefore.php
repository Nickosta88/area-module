<?php

declare(strict_types=1);

namespace Elogic\CustomerArea\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;


class SalesModelServiceQuoteSubmitBefore implements ObserverInterface
{
    /**
     * @param Observer $observer
     * @return $this
     */
    public function execute(Observer $observer)
    {
        $order = $observer->getEvent()->getData('order');
        $quote = $observer->getEvent()->getData('quote');

        $shippingAddressData = $quote->getShippingAddress()->getData();
        if (isset($shippingAddressData['area'])) {
            $order->getShippingAddress()->setArea($shippingAddressData['area']);
        }

        $billingAddressData = $quote->getBillingAddress()->getData();
        if (isset($billingAddressData['area'])) {
            $order->getBillingAddress()->setArea($billingAddressData['area']);
        } else {
            $extAttrs = $quote->getBillingAddress()->getExtensionAttributes();
            if (!empty($extAttrs)) {
                $area = $extAttrs->getArea();
                if ($area) {
                    $order->getBillingAddress()->setArea($area);
                }
            }
        }

        return $this;
    }
}
